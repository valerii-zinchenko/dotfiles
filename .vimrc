" Uncomment the following two lines to enable pathogen package manager
" execute pathogen#infect()
" filetype plugin indent on
set nocompatible              " be iMproved, required
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" plugins
" ------------------------------
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'bling/vim-airline'          " Enable awesome buttom and top bars
Plugin 'Valloric/YouCompleteMe'     " Autocompletion engine: 2016-10-17 it requires vim 7.4.143+
Plugin 'marijnh/tern_for_vim'       " Autocompletion plugin for JavaScript
Plugin 'mattn/emmet-vim'			" Create HTML from CSS rule
Plugin 'tpope/vim-fugitive'         " Git support
Plugin 'scrooloose/nerdtree'        " File explorer
Plugin 'pangloss/vim-javascript'    " JavaScript syntax support
Plugin 'wesQ3/vim-windowswap'       " Swap splitted windows
Plugin 'airblade/vim-gitgutter'     " Highlight changed lines for files in Git repositories
Plugin 'xolox/vim-misc'             " Required for vim sessions
Plugin 'xolox/vim-session'          " Vim sessions
Plugin 'tpope/vim-surround'         " For surroundings: parentheses, brackets, quotes, XML tags, and more

" For previewing markdown files
Plugin 'JamshedVesuna/vim-markdown-preview'
" ------------------------------

" Configure plugins
" ------------------------------
" xolox/vim-session
let g:session_autosave = 'no'

" pangloss/vim-javascript
let g:javascript_plugin_jsdoc = 1

" JamshedVesuna/vim-markdown-preview
let vim_markdown_preview_github = 1
" ------------------------------


call vundle#end()            " required
filetype plugin indent on    " required

" Colors
set t_Co=256 " this enables support of full 256 colors in console. IT IS ROCK with vim-airline
colorscheme elflord
syntax on

" Spaces and tabs
set tabstop=4
"set softtabstop=4
set shiftwidth=4
" Convert tabs into spaces
"set expandtab
" Reset default tabs for specific fyletypes
autocmd FileType sass set noexpandtab

" Display unvisible characters
set list
set listchars=tab:\|\ ,trail:+

" GUI
set number
set relativenumber
set showcmd
filetype indent on
set wildmenu
set showmatch

" Auto reload and write files
set autoread
au FocusGained,BufEnter * :silent! !
au FocusLost,WinLeave * :silent! wa

" Configure CursorLine
set cursorline
highlight CursorLine cterm=none ctermbg=00ff00

" Search
set incsearch
set hlsearch

" Folding
"set foldenable
set foldlevelstart=10
"set foldmethod=indent
set foldmethod=syntax

" Movement
nnoremap j gj
nnoremap k gk
nnoremap gv `[v`]
nnoremap <silent> <A-Left> :execute 'silent! tabmove -1'<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove +1'<CR>

" Enable default configuration for vim-airline
set laststatus=2

" Enable spelling
setlocal spell spelllang=en_us

" Key maps
" ==================================================
" {;
" --> {|};
autocmd FileType * inoremap {; {};<Esc>hi

" [;
" --> [|];
autocmd FileType * inoremap [; [];<Esc>hi

" (;
" --> (|);
autocmd FileType * inoremap (; ();<Esc>hi

" {;<Space>
" --> {};
autocmd FileType * inoremap {;<Space> {};

" [;<Space>
" --> [];
autocmd FileType * inoremap [;<Space> [];

" {,<Space>
" --> {}, |
autocmd FileType * inoremap {,<Space> {},<Space><Space>

" [,<Space>
" --> [], |
autocmd FileType * inoremap [,<Space> [],<Space><Space>

" {<CR>
" --> {
"   |
" }
autocmd FileType * inoremap {<CR> {<CR>}<Esc><S-o>

" {;<CR>
" --> {
"   |
" };
autocmd FileType * inoremap {;<CR> {<CR>};<Esc><S-o>

" [;<CR>
" --> [
"   |
" ];
autocmd FileType * inoremap [;<CR> [<CR>];<Esc><S-o>

" {,<CR>
" --> {
"   |
" },
autocmd FileType * inoremap {,<CR> {<CR>},<Esc><S-o>

" [,<CR>
" --> [
"   |
" ],
autocmd FileType * inoremap [,<CR> [<CR>],<Esc><S-o>
" ==================================================

" .md is for some reason detected as Modula-2 instead of Markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
" make .ts for TypeScript simialr to .js for JavaScript
autocmd BufNewFile,BufReadPost *.ts set filetype=javascript

" Turn off automatic text wrapping by doing 'git commit'
autocmd FileType gitcommit set textwidth=0

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Configure GitGutter
highlight GitGutterAdd ctermfg=2 ctermbg=2
highlight GitGutterChange ctermfg=4 ctermbg=4
highlight GitGutterChangeDelete ctermfg=3 ctermbg=3
